FROM ruby:2.6.6

ENV APP_PATH /www/money_transfer
RUN mkdir -p $APP_PATH

RUN apt-get update -qq \
  && apt-get install -y --no-install-recommends apt-utils cmake

WORKDIR $APP_PATH
COPY Gemfile* ./
RUN bundle install
COPY . .

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["sh","entrypoint.sh"]

EXPOSE 3000
CMD rails s -b 0.0.0.0
