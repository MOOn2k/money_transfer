# README

Solution to the TDS test task.

## Task

```
# Implement TransferService.call to safely transfer money from one user to another. Make sure sender has enough balance.
# Assume this table can be used by other services/applications directly (application level 'mutexes' aren't available).
# Make sure it's safe to execute concurrently in multiple threads/processes.
# User is an ActiveRecord model which properties are annotated below (gem annotate).
# Please cover TransferService with unit tests.
# == Schema Info
#
# Table name: users
#
#  id                  :integer
#  name                :string
#  balance             :decimal(16, 2)
#  created_at          :datetime
#  updated_at          :datetime
#
class User < ApplicationRecord
end
class TransferService
    def self.call(from_user_id, to_user_id, amount)
        # Put your code below:
    end
end
```

## Result files

Service and model:
```
app\models\user.rb
spec\services\transfer_service_spec.rb
```
Tests:
```
app\services\transfer_service.rb
spec\models\user.rb
```
Migration (column types were based on the task description):
```
db\migrate\20210223230154_create_users.rb
```

## You can try it with Docker
1 Start containers
```
docker-compose up -d --build
```
2 Bash into the applications container
```
docker-compose run app bash
```
3 Now you can run the tests
```
bundle exec rspec
```
4 Or even try the service itself (there will be two users created by the seed)
```
bundle exec rails c
sender = User.first
receiver = User.last
result = TransferService.call(from_user_id: sender.id, to_user_id: receiver.id, amount: 20)
receiver.reload.balance.to_s
sender.reload.balance.to_s
```
