class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users, force: true, id: :integer do |t|
      t.string :name, null: false
      t.decimal :balance, precision: 16, scale: 2, default: 0, null: false

      t.timestamps
    end
  end
end
