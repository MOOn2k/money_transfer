#!/bin/sh
rm -f tmp/pids/server.pid
bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:setup db:migrate
bundle exec rake db:seed
exec "$@"