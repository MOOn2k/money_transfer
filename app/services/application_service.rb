# frozen_string_literal: true

require 'dry/monads'
require 'dry-initializer'

class ApplicationService
  include Dry::Monads[:result, :do]
  extend  Dry::Initializer

  def self.call(**args)
    validation = self::Schema.call(args)

    unless validation.success?
      humanized_schema_errors = validation.errors(full: true).to_h.values.flatten
      return Failure.new(humanized_schema_errors)
    end

    new(**args).call
  end
end
