# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'failure schema validation' do
  subject(:call!) { described_class.call(**options) }

  it 'not changing user balance and returns specific failure' do
    expect(call!).to eq Dry::Monads::Failure(error)

    sender.reload
    receiver.reload

    expect(sender.balance).to eq('234.41'.to_d)
    expect(receiver.balance).to eq('1.77'.to_d)
  end
end

RSpec.describe TransferService do
  describe '.call' do
    subject(:instance) { described_class.new(from_user_id: sender&.id, to_user_id: receiver&.id, amount: amount) }

    let(:amount) { BigDecimal('23.65') }
    let(:sender) { create(:user, balance: BigDecimal('234.41')) }
    let(:receiver) { create(:user, balance: BigDecimal('1.77')) }

    context 'when transfer passed successfully' do
      it 'subtracts from senders balance, adds to receivers balance and returns success' do
        expect(instance.call).to be_success

        sender.reload
        receiver.reload

        expect(sender.balance).to eq('210.76'.to_d)
        expect(receiver.balance).to eq('25.42'.to_d)
      end
    end

    context 'when transaction fails' do
      let(:amount) { BigDecimal('500') }
      # before do
      #   allow(instance).to receive(:add_to_receiver).and_raise(ActiveRecord::RecordInvalid)
      # end

      it 'not changing user balance and returns failure' do
        expect(instance.call).to eq Dry::Monads::Failure('failed to save users balance')

        sender.reload
        receiver.reload

        expect(sender.balance).to eq('234.41'.to_d)
        expect(receiver.balance).to eq('1.77'.to_d)
      end
    end

    context 'when sender not found' do
      let(:sender) { nil }

      it 'not changing user balance and returns specific failure' do
        expect(instance.call).to eq Dry::Monads::Failure('sender not found')

        receiver.reload

        expect(receiver.balance).to eq('1.77'.to_d)
      end
    end

    context 'when receiver not found' do
      let(:receiver) { nil }

      it 'not changing user balance and returns specific failure' do
        expect(instance.call).to eq Dry::Monads::Failure('receiver not found')

        sender.reload

        expect(sender.balance).to eq('234.41'.to_d)
      end
    end

    context 'when receiver, sender and amount were passed with wrong types' do
      it_behaves_like 'failure schema validation' do
        let(:options) { { from_user_id: 1.5, to_user_id: '1', amount: 1 } }
        let(:error) { ['from_user_id must be Integer', 'to_user_id must be Integer', 'amount must be BigDecimal'] }
      end
    end

    context 'when passed zero amount' do
      it_behaves_like 'failure schema validation' do
        let(:options) { { from_user_id: sender.id, to_user_id: receiver.id, amount: BigDecimal('0') } }
        let(:error) { ['amount must be greater than 0'] }
      end
    end

    context 'when no params passed' do
      it_behaves_like 'failure schema validation' do
        let(:options) { {} }
        let(:error) { ['from_user_id is missing', 'to_user_id is missing', 'amount is missing'] }
      end
    end

    context 'when insufficient senders balance' do
      let(:amount) { BigDecimal('500') }

      it 'not changing user balance and returns specific failure' do
        expect(instance.call).to eq Dry::Monads::Failure('insufficient senders balance')

        sender.reload
        receiver.reload

        expect(sender.balance).to eq('234.41'.to_d)
        expect(receiver.balance).to eq('1.77'.to_d)
      end
    end
  end
end
