# frozen_string_literal: true

class TransferService < ApplicationService
  option :from_user_id
  option :to_user_id
  option :amount

  Schema = Dry::Schema.Params do
    required(:from_user_id).value(type?: Integer)
    required(:to_user_id).value(type?: Integer)
    required(:amount).value(type?: BigDecimal, gt?: 0)
  end

  def call
    ActiveRecord::Base.transaction do
      sender   = yield find_sender
      receiver = yield find_receiver

      yield validate_balance_sufficiency(sender)

      substract_from_sender(sender)
      add_to_receiver(receiver)

      Success()
    end
  rescue ActiveRecord::RecordInvalid
    Failure('failed to save users balance')
  end

  private

  def find_sender
    find_user(from_user_id, 'sender not found')
  end

  def find_receiver
    find_user(to_user_id, 'receiver not found')
  end

  def find_user(user_id, message)
    user = User.lock.find_by(id: user_id)

    user ? Success(user) : Failure(message)
  end

  def validate_balance_sufficiency(sender)
    amount_is_valid = amount < sender&.balance

    amount_is_valid ? Success() : Failure('insufficient senders balance')
  end

  def substract_from_sender(sender)
    sender.update!(balance: sender.balance - amount)
  end

  def add_to_receiver(receiver)
    receiver.update!(balance: receiver.balance + amount)
  end
end
