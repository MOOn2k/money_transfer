# frozen_string_literal: true

# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  balance    :decimal(16, 2)   default(0.0), not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class User < ApplicationRecord
  validates :name, :balance, presence: true
  validates :balance, numericality: {
    greater_than_or_equal_to: BigDecimal('0'),
    less_than_or_equal_to: BigDecimal('99999999999999.99')
  }
end
